import React, { Component } from 'react'
import dataParser from './data-parser'
import dataManager from './data-manager'

class Edit extends Component {
  state = {
    name:'', 
    shortName:'', 
    idea:'', 
    description:'', 
    targetGroup:'', 
    author:'', 
    links:''
  }

  componentDidMount () {
    this.fetchData(this.props.match.params.id)
  }

  async fetchData (id) {
    const {data} = await dataManager.getApp(id)

    this.setState(data)
  }

  changeProperty = (e) => {
    const newObj = {}
    newObj[e.target.name] = e.target.value
    this.setState(newObj)
  }

  save = (e) => {
    e.preventDefault()
    dataParser.putApp(this.state, this.props.match.params.id)
  }

  render() {
    return (
      <div className="container">
        <h1 className="header-nav page-header" >
          Popraw dane
        </h1>
          <form className="col-md-12">
            <div className="form-group">
              <label>Nazwa</label>
              <input type="text" className="form-control" name="name" placeholder="Nazwa" value={this.state.name} onChange={this.changeProperty}/>
            </div>

            <div className="form-group">
              <label>Skrócona nazwa</label>
              <input type="text" className="form-control" name="shortName" placeholder="Skrócona Nazwa" value={this.state.shortName} onChange={this.changeProperty}/>
            </div>

            <div className="form-group">
              <label>Pomysł</label>
              <textarea className="form-control" rows="3" name="idea" value={this.state.idea} onChange={this.changeProperty}></textarea>
            </div>

            <div className="form-group">
              <label>Opis</label>
              <textarea className="form-control" rows="3" name="description" value={this.state.description} onChange={this.changeProperty}></textarea>
            </div>

            <div className="form-group">
              <label>Grupa docelowa</label>
              <input type="text" className="form-control" name="targetGroup" placeholder="Grupa docelowa" value={this.state.targetGroup} onChange={this.changeProperty}/>
            </div>
            <div className="form-group">
              <label>Autor</label>
              <input type="text" className="form-control" name="author" placeholder="Autor" value={this.state.author} onChange={this.changeProperty}/>
            </div>
            <button type="submit" className="btn btn-primary" onClick={this.save}>Zapisz</button>
          </form>
      </div>
    )
  }
}

export default Edit;
