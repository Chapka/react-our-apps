import React, { Component } from 'react';

class About extends Component {
  render() {
    return (
      <div className="container">
        <h1 className="header-nav page-header" >
          O projekcie
        </h1>
        <div className="card">
          <div className="card-body">
            <h5 className="card-title">Aplikacje Szkoleniowe</h5>
            <h6 className="card-subtitle mb-2 text-muted">Nauka React JS</h6>
            <p className="card-text">Strona poświęcona aplikacjom tworzonym przez zespół w ramach nauki korzystania z bilbioteki React.</p>
            <p className="card-text">W ramach szkolenia ukazały się następujące lekcje:</p>
            <ol>
              <li>Pierwsze kroki - korzystanie ze zbioru 'create-react-app' przy tworzeniu pierwszej aplikacji w React JS: <strong>lesson01/Baby_steps_with_simple_data</strong></li>  
              <li>Stan i właściwości: <strong>lesson02/Properties_or_the_State</strong></li>  
              <li>Różne drogi: <strong>lesson03/Take_the_Route</strong></li> 
              <li>Rdzeń aplikacji: <strong>lesson04/Application_Core</strong></li> 
              <li>Nowa aplikacja: <strong>lesson05/Post_data</strong></li> 
              <li>Aktualizacja wpisu: <strong>lesson06/Update_data</strong></li> 
              <li>Usuwanie: <strong>lesson07/Delete</strong></li> 
            </ol>
          </div>
        </div>
      </div>
    );
  }
}

export default About;
