import axios from 'axios'

const serverAddress = 'http://localhost:3000/'

const getData = (query) => axios.get(serverAddress + query)

const postData = (query, data) => axios.post(serverAddress + query, data)

const putData = (query, data, id) => axios.put(serverAddress + query + '/' + id + '/', data)

const deleteData = (query, id) => axios.delete(serverAddress + query + '/' + id + '/')

const getApps = () => getData('apps')

const getApp = (id) => getData('apps/' + id)

const postApp = (data) => postData('apps', data)

const putApp = (data, id) => putData('apps', data, id)

const deleteApp = (id) => deleteData('apps', id)

export default {
  getData,
  postData,
  putData,
  deleteData,
  getApps,
  getApp,
  postApp,
  putApp,
  deleteApp
}

