import React, { Component } from 'react';

class AppsTable extends Component {
  render() {
    const rows =  this.props.data.map((el,index) => 
      <tr key={index} >
        <th scope="row">{index + 1}</th>
        <td>{el.name}</td>
        <td>
          <button 
            className="btn btn-primary" 
            onClick={() => this.props.changeApp(index)}>
            Podgląd
          </button>
        </td>
      </tr> 
    )

    return (
      <table className="table table-striped">
        <thead>
          <tr key="main">
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col"></th>
          </tr>
        </thead>
        <tbody>
          {rows}           
        </tbody>
      </table>
    );
  }
}

export default AppsTable;
