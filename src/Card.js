import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'
import dataManager from './data-manager'

class Card extends Component {

  render() {
    const {name, idea, description, author, shortName, id} = this.props.element
    return (
      <div className="card">
        <div className="card-body">
          <h5 className="card-title">{name} ({shortName})</h5>
          <p className="card-text">{idea}</p>
          <p className="card-text">{description}</p>
          <p className="card-text author">{author}</p>
        </div>
        <div className="btn-container">
          <NavLink to={'/edit/'+ id} activeClassName="active" className="btn btn-primary">Edytuj</NavLink>
          <button className="btn btn-danger" onClick={() => { this.props.delete(this.props.element.id)}}>Usuń</button> 
        </div>
      </div>
    );
  }
}

export default Card;
