import React, { Component } from 'react';
import App from './App';
import About from './About';
import Add from './Add';
import Edit from './Edit';
import { BrowserRouter, Route, Switch, NavLink } from "react-router-dom";


class Router extends Component {
  render() {
    return (
      <BrowserRouter>
        <div> 
          <nav className="navbar-light bg-light ">
            <div className="nav nav-pills container navbar navbar-expand-lg ">
              <NavLink exact to="/" activeClassName="active" className="nav-link">Główna</NavLink>
              <NavLink to="/apps" activeClassName="active" className="nav-link">Aplikacje</NavLink>
              <NavLink to="/add" activeClassName="active" className="nav-link">Dodaj</NavLink>
            </div>
          </nav>
          <Switch>
            <Route path="/" exact component={About} />
            <Route path="/apps/" exact component={App} />
            <Route path="/add/" exact component={Add} />
            <Route path="/edit/:id" exact component={Edit} />
          </Switch>
        </div>
      </BrowserRouter>
    )
  }
}

export default Router;
