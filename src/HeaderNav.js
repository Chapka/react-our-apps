import React, { Component } from 'react';

class HeaderNav extends Component {
  render() {
    const buttons =  this.props.data.map((el,index) => 
      <button 
        className="btn btn-primary" 
        key={index} 
        onClick={() => this.props.changeApp(index)}>
        {el.name}
      </button>  
    )

    return (
      <div className="header-nav" >
        {buttons}
      </div>
    );
  }
}

export default HeaderNav;
