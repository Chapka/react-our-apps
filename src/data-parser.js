import dataManager from './data-manager'
import uuidv1 from 'uuid/v1'

const postApp = ({name="", shortName="", idea="", description="", links=[], targetGroup="", author=""}) => {
  console.log('shortName', shortName)
  const parsedData = {
    id: uuidv1(),
    name,
    shortName,
    idea,
    description,
    links,
    targetGroup,
    author
  }
  dataManager.postApp(parsedData)
}
const putApp = ({name="", shortName="", idea="", description="", links=[], targetGroup="", author=""}, id) => {
  const parsedData = {
    name,
    shortName,
    idea,
    description,
    links,
    targetGroup,
    author
  }
  dataManager.putApp(parsedData, id)
}

export default {
  postApp,
  putApp
}