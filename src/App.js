import React, { Component } from 'react';
import './App.css';
import HeaderNav from './HeaderNav'
import AppsTable from './AppsTable'
import Card from './Card'
import dataManager from './data-manager'


class App extends Component {
	constructor(props) {
    super(props)
    this.state = {
      index: 0,
      data: [
        {
          name:'', 
          shortName:'', 
          idea:'', 
          description:'', 
          targetGroup:'', 
          author:'', 
          links:''
        }
      ]
    }
  }

  componentDidMount () {
    dataManager.getApps()
    .then(({data}) => {
      this.setState({
        data
      })  
    })
  }

  changeApp = (index) => {
    this.setState(state => ({
      index
    }));
  }

  deleteApp = (id) => {
    dataManager.deleteApp(id)
    const newStateData = this.state.data.filter(app => {
      if (app.id === id) {
        return false
      }
      return true
    })
    this.setState({
      data: newStateData,
      index: 0
    })
  }

  render() {
  return (
    <div>
      <div className="container pt-30">
        <div className="row">
          <div className="col col-sm-6">
            <AppsTable data={this.state.data} changeApp={this.changeApp} />
          </div>
          <div className="col col-sm-6">
            <Card element={this.state.data[this.state.index]} delete={this.deleteApp} />
          </div>
        </div>
      </div>
      <div className="modal show fade" tabIndex="-1" role="dialog">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title">Modal title</h5>
              <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              <p>Modal body text goes here.</p>
            </div>
            <div className="modal-footer">
              <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="button" className="btn btn-primary">Save changes</button>
            </div>
          </div>
        </div>
      </div>
    </div>
    );
  }
}

export default App;
